<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/6/16
 * Time: 3:19 PM
 */

define('WP_USE_THEMES', false);
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
require(__DIR__.'/wp-load.php');

function saveProduct($row)
{
    $productName = str_replace('*', '', $row['name']);
    $productCategory = strtolower($row['category']);
    $productCategory = str_replace('ny stone', '', $productCategory);
    $productCategory = trim($productCategory);

    $productSubCategory = strtolower($row['subCategory']);
    $productSubCategory = trim($productSubCategory);

    if($productCategory){
        if($productSubCategory == 'slab' || $productSubCategory == 'tile'){
            $productSubCategory = $productSubCategory.'s'.'-'.$productCategory;
        }
    }
    $productImage = $row['image'];
//    $length = $row['Length'];
//    $width = $row['Width'];
//    $lotsNumber = $row['IDOne'];
    $options = $row['options'];

    $product = [
        'post_title' => $productName,
        'post_status' => 'publish',
        'post_type' => 'product'
    ];

    $page = get_page_by_title($productName, OBJECT, 'product');

    if($page && $page->ID){
        $product['ID'] = $page->ID;
    }

    $productId = wp_insert_post($product);

    if($productImage){
        $filename = $productImage;

        $basePath = __DIR__.'/wp-content/uploads/';
        $savePath = '2016/04';

        file_put_contents($basePath.$savePath.'/'.basename($filename), file_get_contents($filename));

        $filename = $savePath.'/'.basename($filename);

        $wp_filetype = wp_check_filetype(basename($filename), null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $filename, $productId );
    }


    if($productId){
        if(isset($attach_id)){
            add_post_meta($productId, '_thumbnail_id', $attach_id, true);
        }

        add_post_meta($productId, 'options', json_encode($options), true);

        $term = get_term_by('slug', $productCategory, 'product_cat');
        if($productCategory){
            $subCatTerm = get_term_by('slug', $productSubCategory, 'product_cat');
        }


        if($term && $term->term_id){
            wp_set_object_terms($productId, $term->term_id, 'product_cat');
            if($productCategory){
                wp_set_object_terms($productId, $subCatTerm->term_id, 'product_cat', true);
            }
        }
    }
}

$myServer = "67.202.78.101";
$myUser = "nysuser";
$myPass = "nys@u!s@er";
$myDB = "R_WebConnectItem_InventoryImagePath";

//connection to the database
//$dbhandle = mssql_connect($myServer, $myUser, $myPass)
//or die("Couldn't connect to SQL Server on $myServer");

//declare the SQL statement that will query the database
$query = "SELECT TOP 100 ItemID, ItemName, IDOne, Barcode, Length, Width, Category, Type, InventoryImage, ItemImage FROM R_WebConnectItem_InventoryImagePath WHERE IDOne IS NOT NULL AND IDOne != ''";

$myServer = "67.202.78.101";
$myUser = "nysuser";
$myPass = "nys@u!s@er";
$myDB = "R_WebConnectItem_InventoryImagePath";


$dsn= "dblib:host=$myServer:1433;";
$mspdo = new PDO($dsn,$myUser,$myPass);
$mspdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$result = $mspdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
var_dump(count($result));
die();
//execute the SQL query and return records
//$result = mssql_query($query);

//display the results
$data = [];
foreach($result as $key => $row)
{
    $name = trim($row['ItemName']);
    $lots = trim($row['IDOne']);
    $length = trim($row['Length']);
    $width = trim($row['Width']);
    $img = trim($row['InventoryImage']);
    $cat = trim($row['Category']);
    $subCat = trim($row['Type']);
    $barcode = trim($row['Barcode']);
    $itemImage = trim($row['ItemImage']);


    if($length && $width){
        $sizeText = "$length x $width";
    } else {
        $sizeText = "";
    }
    $data[$name]['options'][$lots][] = ['size' => $sizeText, 'image' => $img, 'lots' => $lots, 'barcode' => $barcode];
    $data[$name]['category'] = $cat;
    $data[$name]['name'] = $name;
    $data[$name]['image'] = $itemImage?$itemImage:$img;
    $data[$name]['subCategory'] = $subCat;
}

//var_dump($data); die();
//foreach($data as $row){
//    foreach($row['options'] as $lots => $option){
//        foreach($option as $info){
//
//        }
//    }
//}

foreach($data as $row){
    echo "Processing product {$row['name']} \n";
    saveProduct($row);
}

//close the connection
//mssql_close($dbhandle);
