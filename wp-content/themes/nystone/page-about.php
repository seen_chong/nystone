<?php get_header(); ?>
		<div class="aboutMainHero">
			<div class="media" style="background-image:url('<?php the_field('main_image'); ?>')"></div>
			<div class="aboutMainHero_text">
				<div class="about_content_wrapper">
					<h1 class="futura mainTitle">About</h1>
					<h2 class="futura subTitle">We will bring your design dreams to reality.</h2>
					<p><?php the_field('about_text'); ?></p>
				</div>
			</div>
		</div>
		<div class="aboutServicesSection">
			<div class="row_of_2">
				<div class="col">
					<h4 class="futura">Cut to Size</h4>
					<p><?php the_field('cut_to_size'); ?></p>
				</div>
				<div class="col">
					<h4 class="futura">Custom Pieces</h4>
					<p><?php the_field('custom_pieces'); ?></p>
				</div>
			</div>		
			<div class="row_of_3">
				<div class="col">
					<h4 class="futura">Slabs</h4>
					<p><?php the_field('slabs'); ?></p>
				</div>
				<div class="col">
					<h4 class="futura">Tiles</h4>
					<p><?php the_field('tiles'); ?></p>
				</div>
				<div class="col">
					<h4 class="futura">Mosaics</h4>
					<p><?php the_field('mosaics'); ?></p>
				</div>
			</div>
			<div class="row">
				<p class="caption"><?php the_field('bottom_text'); ?></p>
			</div>
		</div>
		<div class="aboutTransportationSection">
			<div class="row">
				<h4 class="futura">Custom Order</h4>
				<div class="centeredRow">
					<div class="iconText col">
						<div class="iconMedia" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/containerBoatIcon.jpg')"></div>
						<p>Container Boat</p>
					</div>
					<div class="iconText col">
						<div class="iconMedia" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/airFreightIcon.jpg')"></div>
						<p> AirFreight</p>
					</div>
				</div>
			</div>
			<div class="row">
				<h4 class="futura">Stock Order</h4>
				<div class="centeredRow">
					<div class="iconText col">
						<div class="iconMedia" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/truckingIcon.jpg')"></div>
						<p>Trucking</p>
					</div>
					<div class="iconText col">
						<div class="iconMedia" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/mailIcon.jpg')"></div>
						<p>By Mail</p>
					</div>
					<div class="iconText col">
						<div class="iconMedia" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/pickupIcon.jpg')"></div>
						<p>Pick Up</p>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>
