<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="product_detail_wrapper">
<script>
	$(function(){
		$("#product_detail_swatch").imageLens({ lensSize: 200,borderColor:"#2D2F33" });
	});	
</script>
<div class="left_column">
<div class="left_on_690">
	<div class="product_detail_swatch_mask">

	<?php
		if ( has_post_thumbnail() ) {

			$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
			$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
			$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	=> $image_title,
				'alt'	=> $image_title,
				'class' => 'product_detail_swatch',
				'id' => 'product_detail_swatch'
				) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_caption, $image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>
		</div>
			<?php
				$other_page = get_field('parent_product_id');
			?>
			<p><b>Name:</b> <?php the_title(); ?></p>
			<p><b>Type:</b> <?php $type = null; foreach(wp_get_post_terms($post->ID, 'product_cat') as $cat){ $type = $type. $cat->name .' ';} echo $type;?></p>
			<p><b>Thickness:</b> <?php $names = explode(' ', $post->post_title); if(strpos($names[0], 'CM') !== false) echo $names[0]; else echo $names[count($names) - 1].' INCH'; ?></p>
		</div>

		<div class="pantone_product_detail_container">
<!-- 			<div class="pantone_logo"></div> -->
			<div class="pantone_thumbnails_wrapper">
<!-- 				<div class="pantone_thumb" style="background:#a0ab4c"></div>
				<div class="pantone_thumb" style="background:#9bae89"></div>
				<div class="pantone_thumb" style="background:#5d7975"></div>
				<div class="pantone_thumb" style="background:#233745"></div>
				<div class="pantone_thumb" style="background:#c3d7ee"></div> -->
			</div>
		</div>

		<div class="right_on_690">
			<div class="paragraph_with_header">
				<p><b>Material Description:</b></p>
				<p><?php the_field('description', $other_page); ?></p>
			</div>
			<div class="product_detail_actions">
				<a class="primary_btn toggle_full_screen_modal" data-full_screen_modal_type="request_sample">Sample Request</a>
				<a class="primary_btn toggle_full_screen_modal" data-full_screen_modal_type="bookmatch">Bookmatch</a>
				<a class="primary_btn toggle_full_screen_modal like_btn" data-full_screen_modal_type="register">
					<i class="fa fa-thumbs-o-up"></i>
				</a>
				<br>
				<!-- <a class="clear_btn">Download Material Specification</a> -->
				<!-- <span>|</span> -->
				<?php echo do_shortcode("[yith_wcwl_add_to_wishlist]"); ?>
				<?php echo do_shortcode( '[woocommerce_social_media_share_buttons]' ); ?>
			</div>
		</div>
	</div>
					<div class="right_column">
<!-- 						<div class="lots_wrapper">
							<div class="lots_rows" id="lots_scroll_wrapper">

							</div> -->

							<!-- static content -->
			<?php if($options = get_field('options')){ ?>
			<div class="items-slider-container js-items-slider-container">
		        <div class="slider slider-for">
					<?php
					$options = json_decode($options);
					foreach($options as $lots => $option){ $sizePool = [];?>
						<div>
							<a href="<?=$option[0]->image?>" rel="lightbox">
								<img src="<?=$option[0]->image?>" style="cursor:zoom-in;">
							</a>
							<div class="item-informations-container left-top">
								<div class="item-slide-title">Lots#: <?=$lots?></div>
								<?php foreach($option as $data){ ?>
									<?php if(!in_array($data->size, $sizePool) && $data->size){
									$sizePool[] = $data->size; ?>
									<?php } ?>
								<?php } ?>
								<div class="item-slide-info-1">Dimension: <?=$sizePool?implode(",", $sizePool): "Ask For Info"?></div>
							</div>
						</div>
					<?php } ?>
		        </div>

		        <div class="slider slider-nav">
					<?php foreach($options as $optionAlt){ ?>
		            <div><img src="<?=$optionAlt[0]->image?>" alt=""></div>
		            <?php } ?>
		        </div>

		    </div>
			<?php } else { ?>
				<?php
				// find same name products
				function title_filter( $where, &$wp_query )
				{
					global $wpdb;
					if ( ($search_term = $wp_query->get( 'search_prod_title' )) &&  ($searchId = $wp_query->get( 'search_prod_id' ))) {
						$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\''. ' AND ID != '.$searchId;
					}
					return $where;
				}

				$args = array(
					'post_type' => 'product',
					'post_status'      => 'publish',
					'search_prod_title' => the_title('','', false),
					'search_prod_id' => get_the_ID(),
					'post_status' => 'publish',
					'orderby'     => 'title',
					'order'       => 'ASC'
				);

				add_filter( 'posts_where', 'title_filter', 10, 2 );
				$wp_query = new WP_Query($args);
				remove_filter( 'posts_where', 'title_filter', 10, 2 );
				if($wp_query->posts) {
					$optionsArray = [];
					foreach ($wp_query->posts as $loop => $asscPost) {
						if ($options = get_field('options', $asscPost->ID)) {
							$optionsArray[] = $options;
						}
					}
				}
				?>
			<? } ?>
				<?if($optionsArray){ ?>
				<div class="items-slider-container js-items-slider-container">
					<div class="slider slider-for">

						<?php
						foreach($optionsArray as $options){
							$options = json_decode($options);
							foreach($options as $lots => $option){ $sizePool = [];?>
								<div>
									<a href="<?=$option[0]->image?>" rel="lightbox">
										<img src="<?=$option[0]->image?>" style="cursor:zoom-in;">
									</a>
									<div class="item-informations-container left-top">
										<div class="item-slide-title">Lots#: <?=$lots?></div>
										<?php foreach($option as $data){ ?>
											<?php if(!in_array($data->size, $sizePool) && $data->size){
												$sizePool[] = $data->size; ?>
											<?php } ?>
										<?php } ?>
										<div class="item-slide-info-1">Dimension: <?=$sizePool?implode(",", $sizePool): "Ask For Info"?></div>
									</div>
								</div>
							<?php } ?>
						<? } ?>
					</div>
					<div class="slider slider-nav">
						<?php foreach($optionsArray as $optionAltArray){ $optionAltArray = json_decode($optionAltArray); foreach($optionAltArray as $optionAlt) { ?>
							<div><img src="<?=$optionAlt[0]->image?>" alt=""></div>
						<?php } }  ?>
					</div>
				</div>
				<? } ?>
		</div>
	</div>
</div> 


<!-- popup modals -->
		<div class="request_sample_modal full_screen_modal dark_background_full_screen_modal" id="request_sample_full_screen_modal">
			<div class="inner_modal_box">
				<a class="right_alligned_close_modal close_full_screen_modal">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="69 47.5 64.5 64.5" enable-background="new 69 47.5 64.5 64.5" xml:space="preserve">
						<g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M127.1,51.9l1,1c0,0,0,0.1,0,0.1L73,108.1c0,0-0.1,0-0.1,0
									l-1-1c0,0,0-0.1,0-0.1L127.1,51.9C127,51.9,127.1,51.9,127.1,51.9z"/>
							</g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M73,51.9l55.1,55.1c0,0,0,0.1,0,0.1l-1,1c0,0-0.1,0-0.1,0
									L71.9,53c0,0,0-0.1,0-0.1L73,51.9C72.9,51.9,73,51.9,73,51.9z"/>
							</g>
						</g>
					</svg>
					<p>(esc)</p>
				</a>
				<h1>Sign up for Sample Request</h1>
				<?php echo do_shortcode( '[contact-form-7 id="239" title="Request A Sample"]' );?>
			</div>
		</div>

		<div class="request_sample_modal full_screen_modal dark_background_full_screen_modal" id="register_full_screen_modal">
			<div class="inner_modal_box">
				<a class="right_alligned_close_modal close_full_screen_modal">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="69 47.5 64.5 64.5" enable-background="new 69 47.5 64.5 64.5" xml:space="preserve">
						<g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M127.1,51.9l1,1c0,0,0,0.1,0,0.1L73,108.1c0,0-0.1,0-0.1,0
									l-1-1c0,0,0-0.1,0-0.1L127.1,51.9C127,51.9,127.1,51.9,127.1,51.9z"/>
							</g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M73,51.9l55.1,55.1c0,0,0,0.1,0,0.1l-1,1c0,0-0.1,0-0.1,0
									L71.9,53c0,0,0-0.1,0-0.1L73,51.9C72.9,51.9,73,51.9,73,51.9z"/>
							</g>
						</g>
					</svg>
					<p>(esc)</p>
				</a>
				<h1>Create Account</h1>
				<p>(to add products to your favorites)</p>
				<form class="request_sample_form">
					<?php echo do_shortcode( '[profilepress-registration id="1"]' );?>
					
				</form>
			</div>
		</div>

		<div class="bookmatch_sample_modal full_screen_modal dark_background_full_screen_modal" id="bookmatch_full_screen_modal">
			<div class="inner_modal_box bookmatch_modal_box" style="max-width:100%;">
				<a class="right_alligned_close_modal close_full_screen_modal">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="69 47.5 64.5 64.5" enable-background="new 69 47.5 64.5 64.5" xml:space="preserve">
						<g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M127.1,51.9l1,1c0,0,0,0.1,0,0.1L73,108.1c0,0-0.1,0-0.1,0
									l-1-1c0,0,0-0.1,0-0.1L127.1,51.9C127,51.9,127.1,51.9,127.1,51.9z"/>
							</g>
							<g>
								<path stroke="#ffffff" stroke-width="3" stroke-miterlimit="10" d="M73,51.9l55.1,55.1c0,0,0,0.1,0,0.1l-1,1c0,0-0.1,0-0.1,0
									L71.9,53c0,0,0-0.1,0-0.1L73,51.9C72.9,51.9,73,51.9,73,51.9z"/>
							</g>
						</g>
					</svg>
					<p>	<i>for reference only</i> (esc)</p>
				</a>
					<?php
		if ( has_post_thumbnail() ) {

			$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
			$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
			$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	=> $image_title,
				'alt'	=> $image_title,
				'class' => 'product_detail_swatch',
				'id' => 'product_detail_swatch'
				) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_caption, $image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>

		<?php
		if ( has_post_thumbnail() ) {

			$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
			$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
			$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	=> $image_title,
				'alt'	=> $image_title,
				'class' => 'product_detail_swatch',
				'id' => 'product_detail_swatch'
				) );

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="mirror_img" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_caption, $image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>
			</div>
		</div>

	<?php do_action( 'woocommerce_product_thumbnails' ); ?>

</div>

<style type="text/css">
	img.ajax-loader {
		display: none;
	}

</style>

<script type="text/javascript">
$(document).ready( function () {

   // Sliders

   //// Slider Top
  $('.js-items-slider-container .slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.js-items-slider-container .slider-nav'
  });
  $('.js-items-slider-container .slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.js-items-slider-container .slider-for',
      dots: true,
      centerMode: false,
      focusOnSelect: true,
      arrows: true,
      accessibility: true,
      onAfterChange: function (slide, index) {
        console.log("slider-nav change");
        console.log(this.$slides.get(index));
        $('.current-slide').removeClass('current-slide');
        $(this.$slides.get(index)).addClass('current-slide');
      },
      onInit: function (slick) {
        $(slick.$slides.get(0)).addClass('current-slide');
      }
  });
});
</script>