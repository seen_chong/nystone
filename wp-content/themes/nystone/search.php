<?php get_header(); ?>


		<div class="search_results_content_wrapper tax-product_color">
			<div class="search_filter_row">
				<!-- <div class="left filter_options_wrapper">
					<div class="option_button filter_option_btn">
						<p>Slab</p>
						<input type="checkbox"/>
					</div>
					<span>/</span>
					<div class="option_button filter_option_btn">
						<p>Tile</p>
						<input type="checkbox"/>
					</div>
				</div> -->
				<h1 title="advanced_search">Advanced Search</h1>
			</div>

			<div class="search_filter_row">
							<?php
			if(is_active_sidebar('widget-area-2')){
				dynamic_sidebar('widget-area-2');
			}
			?>
<!-- 				<div class="pantone_swatches_wrapper">
					<div id="colorpicker-layout" value=""></div>
					<input id="colorpicker-selected-value" value="No colors selected" type="text">
				</div> -->
			</div>
			<div class="search_filter_row">
				<div class="filter_options_wrapper filter_flex_slider">
					<div class="option_button filter_option_btn" data-material="marble">
						<div class="filter_color"></div>
						<p>
							<span>Marble</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="granite">
						<div class="filter_color"></div>
						<p>
							<span>Granite</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="limestone">
						<div class="filter_color"></div>
						<p>
							<span>Limestone</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="quartzite">
						<div class="filter_color"></div>
						<p>
							<span>Quartzite</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="travertine">
						<div class="filter_color"></div>
						<p>
							<span>Travertine</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="onyx">
						<div class="filter_color"></div>
						<p>
							<span>Onyx</span>
						</p>
						<input type="checkbox"/>
					</div>
					<div class="option_button filter_option_btn" data-material="porcelain">
						<div class="filter_color"></div>
						<p>
							<span>Porcelain</span>
						</p>
						<input type="checkbox"/>
					</div>
				</div>
			</div>

			<div class="search_filter_row search_input_row">
				<input class="primary_btn" type="submit" id="search-submit" value="Search">
			</div>

			<div class="search_filter_row product_results">

				<?php get_template_part('search-loop'); ?>

				<?php get_template_part('pagination'); ?>

			</div>


		</div>

<script>
	$(document).ready(function(){
		function getParameterByName(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		// material highlight slick-slide slick-active selected
		// color highlight
		$("#search-submit").on('click', function(e){
			e.preventDefault();
			var selectedMaterial = [];
			var selectedColor = [];
			$(".search_filter_row").find('.slick-active.selected').each(function(){
				selectedMaterial.push($(this).data('material'));
			});

			$(".color-item").find('.color-hover').each(function(){
				selectedColor.push($(this).data('color'))
			});

			var redirectString = '/?s=' + getParameterByName('s');
			redirectString += ('&color=' + selectedColor.join());
			redirectString += ('&material=' + selectedMaterial.join());
			window.location.href = redirectString;
		});

//		$('.filter_option_btn').on('click', function(){
//			console.log('material clicked');
//		});

		$(".rcorners").on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('color-hover');
		});

		var colors = getParameterByName('color');
		var materials = getParameterByName('material');

		if(colors){
			var colorsArray = colors.split(",");
			for(var i in colorsArray){
				$('*[data-color="'+ colorsArray[i]+'"]').addClass('color-hover');
			}
		}

		if(materials){
			var materialArray = materials.split(",");
			for(var i in materialArray){
				$('*[data-material="'+ materialArray[i]+'"]').addClass('slick-slide slick-active selected');
			}
		}


	})
</script>
<?php get_footer(); ?>

