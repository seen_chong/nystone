<?php get_header(); ?>

	<div class="full_width_hero full_width_hero_thin" style="background-image:url(<?php the_field('main_image'); ?>)">
	</div>
	<div class="content_wrapper">
			<div class="about_content_wrapper">
				<h1 class="futura"><?php the_title(); ?></h1>
				<p><?php the_field('about_text'); ?></p>

				<div class="about_location_content_wrapper" id="showroom">
					<div class="single_item_slider">

								<?php
								  $args = array(
								    'post_type' => 'showroom'
								    );
								  $products = new WP_Query( $args );
								  if( $products->have_posts() ) {
								    while( $products->have_posts() ) {
								      $products->the_post();
								?>

						<div class="full_size_slider_item" style="background-image:url(<?php the_field('showroom_image'); ?>)">
						</div>

								<?php
							    		}
							  		}
								  else {
								    echo 'No Showrooms Found';
								  }
							  	?>

					</div>
					<h2>Showroom</h2>
					<em>30 W. 21st Street</em>	
					<?php
					  $args = array(
					    'name' => '$showroom-text'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>	

					<p><?php the_field('showroom_text'); ?></p>

					<?php
				    		}
				  		}
					  else {
					    echo 'No Showrooms Found';
					  }
					  ?>
					<a href="/contact" class="clear_btn" style="text-decoration:none;"><span class="fa fa-map-marker"></span>Visit</a>
				</div>

				<div class="about_location_content_wrapper" id="warehouse">
					<div class="single_item_slider">

							<?php
								  $args = array(
								    'post_type' => 'warehouses'
								    );
								  $products = new WP_Query( $args );
								  if( $products->have_posts() ) {
								    while( $products->have_posts() ) {
								      $products->the_post();
								?>

						<div class="full_size_slider_item" style="background-image:url(<?php the_field('warehouse_image'); ?>)">
						</div>

							<?php
							    		}
							  		}
								  else {
								    echo 'No Warehouses Found';
								  }
							  	?>
					</div>
					<h2>Warehouse</h2>
					<em>New Jersey</em>			
					<?php
					  $args = array(
					    'name' => '$warehouse-text'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>			

					<p><?php the_field('warehouse_text'); ?></p>

					<?php
				    		}
				  		}
					  else {
					    echo 'No Warehouses Found';
					  }
				  	?>

					<a href="/contact" class="clear_btn" style="text-decoration:none;"><span class="fa fa-map-marker"></span>Visit</a>

				</div>
				<!-- <h4>Why Work with Us?</h4>
				<div class="why_work_with_us" id="whyus">
					<h1><b>HIGHLY CURATED</b> SELECTION OF NATURAL STONE</h1>
					<p>Diverse selections of Marble, Granite, Quartzite, Limestone, & Travertine.</p>

					<h1><b>QUALITY</b> SUPPLIER RELATIONSHIPS</h1>
					<p>Access to our senior management, product specialists, logistics professionals, operations staff.</p>
					<p>Our supplier database contains over 100 worldwide.</p>

					<h1><b>INTERNATIONALLY</b> & DOMESTICALLY SOURCED MATERIALS</h1>
					<p>New York Stone sources directly from quarries in Italy, Brazil, Turkey, Spain, and China. We also source domestically.</p>
				</div>
				<div class="two_column_wrapper" id="press">
					<h1>Press</h1>

					<//?php
					  $args = array(
					    'post_type' => 'presslinks'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>


					<a class="client_logo_link" href="<//?php the_field('link'); ?>" target="blank">
						<div>
							<div class="background_img" style="background-image:url(<//?php the_field('client_logo'); ?>)"></div>
							<img src="<//?php the_field('client_logo'); ?>"/>
						</div>
					</a>

					<//?php
				    		}
				  		}
					  else {
					    echo 'No Warehouses Found';
					  }
				  	?>


				</div> -->
			</div>
		</div>

<?php get_footer(); ?>
