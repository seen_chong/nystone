<?php
/*
Template Name Posts: Products
*/
?>


<?php get_header(); ?>

		<div class="content_wrapper project_detail_content_wrapper">
			<div class="project_detail_banner_wrapper">
				<div class="img" style="background-image:url(<?php the_field('header_image'); ?>)"></div>
				<div class="img_overlay"></div>
				<!-- <p>Travertine</p> -->
			</div>		
			<div class="projects_content_wrapper">
				<div class="thin_inner_nav">
					<div class="left white_breadcrumbs" id="breadcrumbs">
						<a href="/projects">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 34.7 50.8" enable-background="new 0 0 34.7 50.8" xml:space="preserve">
								<polyline fill="#53585F" points="33.1,1 1.1,25 33.1,49 33.1,42.8 9.6,25 33.1,7.2 "/>
							</svg>
							<p>Projects</p>
						</a>
						<span>/</span>
						<a><p><?php the_title(); ?></p></a>
					</div>
					<h2 class="nav_page_title">Projects</h2>
				</div>
				<div class="project_detail_wrapper">
					<div class="single_project_address_1">
						<p><?php the_field('name'); ?></p>
					</div>
					<div class="single_project_address_2">
						<p><?php the_field('location'); ?></p>
					</div>

				<div class="single_item_slider project_images_slider">

					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image'); ?>)">
					</div>

				<?php if (get_field('image_2') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_2'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_3') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_3'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_4') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_4'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_5') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_5'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_6') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_6'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_7') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_7'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_8') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_8'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_9') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_9'); ?>)">
					</div>
				<?php endif; ?>

				<?php if (get_field('image_10') != ''): ?>
					<div class="full_size_slider_item" style="background-image:url(<?php the_field('image_10'); ?>)">
					</div>
				<?php endif; ?>

				</div>
					<div class="projects_details_data">
						<ul>
							<li><p>Architect: <?php the_field('architect'); ?></p></li>
							<li style="text-align:right;"><p>Developer: <?php the_field('developer'); ?></p></li>
						</ul>
						
					</div>
					<div class="project_summary_view ">
						<p><?php the_field('description'); ?></p>

						<div class="project_text_box_wrapper">
							<div class="project_text_box">
								<ul>
									<li>Client: <?php the_field('name'); ?></li>
									<li>Materials: <?php the_field('material'); ?></li>
									<li>Architect: <?php the_field('architect'); ?></li>
									<li>Developer: <?php the_field('developer'); ?></li>
									<li>Scale: <?php the_field('scale'); ?></li>
								</ul>
							</div>
						</div>

						<div class="client_logos_content_wrapper" id="project_client_logos">

							<a class="client_logo_link">
								<div>
									<div class="background_img" style="background-image:url(<?php the_field('logo_1'); ?>)"></div>
									<img src="<?php the_field('logo_1'); ?>"/>
								</div>
							</a>
							<a class="client_logo_link">
								<div>
									<div class="background_img" style="background-image:url(<?php the_field('logo_2'); ?>)"></div>
									<img src="<?php the_field('logo_2'); ?>"/>
								</div>
							</a>
							<a class="client_logo_link">
								<div>
									<div class="background_img" style="background-image:url(<?php the_field('logo_3'); ?>)"></div>
									<img src="<?php the_field('logo_3'); ?>"/>
								</div>
							</a>
							<a class="client_logo_link">
								<div>
									<div class="background_img" style="background-image:url(<?php the_field('logo_4'); ?>)"></div>
									<img src="<?php the_field('logo_4'); ?>"/>
								</div>
							</a>
							<a class="client_logo_link">
								<div>
									<div class="background_img" style="background-image:url(<?php the_field('logo_4'); ?>)"></div>
									<img src="<?php the_field('logo_4'); ?>"/>
								</div>
							</a>

						</div>
					</div>

					<div class="project_full_view">

						<div class="project_full_view_left">
							<div class="project_full_view_header">
								<h2 class="futura"><?php the_title(); ?></h2>
							</div>
							<div class="project_full_view_details">
								<ul>
									<li>Architect: <?php the_field('architect'); ?></li>
									<li>Developer: <?php the_field('developer'); ?></li>
									<li>Interiors: <?php the_field('interiors'); ?></li>
									<li>Materials: <?php the_field('material'); ?></li>
									<li>Scale: <?php the_field('scale'); ?></li>
								</ul>

								<p><?php the_field('summary_details'); ?></p>

								<ul>
									<li><?php the_field('name'); ?></li>
									<li><?php the_field('location'); ?></li>
								</ul>
							</div>
						</div>

						<div class="project_full_view_right">
							<img src="<?php the_field('summary_image'); ?>">
							<h6 class="futura">Completion in <?php the_field('completion_date'); ?></h6>
						</div>

					</div>

				</div>
			</div>
		</div>

<?php get_footer(); ?>
