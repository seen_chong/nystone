<?php get_header(); ?>

		<div class="clientsPageMainWrapper">
			<h1 class="futura mainTitle">Clients</h1>
			<div class="row row_of_3 row_logos">

 				    <?php
					  $args = array(
					    'post_type' => 'clients'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>

				<div class="col">
					<img src="<?php the_field('client_logo'); ?>">
				</div>



 				    <?php
				    		}
				  		}
					  else {
					    echo 'No Job Listings Found';
					  }
				  	?>
			</div>
		</div>

<?php get_footer(); ?>
