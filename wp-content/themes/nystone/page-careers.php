<?php get_header(); ?>

		<div class="careersPageMainWrapper">
			<h1 class="futura mainTitle">Careers at New York Stone</h1>
			<h2 class="futura subTitle">Congratulations! We are looking to hire!</h2>
			<h2 class="futura subTitle">Find instructions below on how to contact us and submit a job application. We are currently looking for people in the following positions:</h2>
			<div class="row row_of_3 row_career_options">

				    <?php
					  $args = array(
					    'post_type' => 'careers'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>

				<div class="col">
					<a>
						<p class="futura"><?php the_field('job_position'); ?></p>
					</a>
				</div>

				    <?php
				    		}
				  		}
					  else {
					    echo 'No Job Listings Found';
					  }
				  	?>
			</div>
		</div>

<?php get_footer(); ?>
