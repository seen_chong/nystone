<?php //if (have_posts()): while (have_posts()) : the_post(); ?>

<!-- article -->
<?php
$postsArray = [];

if(have_posts()){
    while (have_posts()){
        the_post();
        $postsArray[] = get_post();
    }
}

$colorsArray = [];
$materialsArray = [];

$hasColor =  isset($_GET['color']) && $_GET['color'];
$hasMaterial =  isset($_GET['material']) && $_GET['material'];
//var_dump(the_search_query());
//die();
$matches = [];
if($_GET['t'] =='slot'){
    $matches = get_posts(array(
        'posts_per_page' => -1,
        'post_type' => 'product',
        'post_status' => array('draft'),
        'meta_query' => array(
            array(
                'key' => 'options',
                'value' => $_GET['s'],
                'compare' => 'LIKE',
            )
        ),
    ));

    if($matches){
        $newProducts = [];
        $searchName = [];
        foreach($matches as $subMatch){
            $search = null;
            $arrayTitles = explode(' ', $subMatch->post_title);
            if(strpos($arrayTitles[0], 'X') !== false || strpos($arrayTitles[0], 'CM') !== false){
                $arrayTitles[0] = '';
            }

            if(strpos($arrayTitles[count($arrayTitles) - 1], '/') !== false){
                $arrayTitles[count($arrayTitles) - 1] = '';
            }

            $search = implode(' ', $arrayTitles);
            $search = trim($search);

            $searchName = array_merge($searchName, [$search]);
        }

        $searchName = array_unique($searchName);

        if(count($searchName) >= 3){
            $searchParmas = implode(',', array_slice($searchName,0, 3));
        } else {
            $searchParmas = implode(',', $searchName);
        }

        header( 'Location: http://newyorkstone.com?s='.$searchParmas.'&t=product');
        die();
    }

    $postsArray = $matches;


} elseif($_GET['t'] == 'product'){
    if(isset($_GET['color']) && isset($_GET['material'])){
        $colors = explode(',', $_GET['color']);
        $materials = explode(',', $_GET['material']);

        foreach($colors as $color){
            if(!$color){
                continue;
            }
            $args = array(
                'posts_per_page'   => -1,
                'offset'           => 0,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'product',
                'post_status'      => 'publish',
                'tax_query' => [['taxonomy' => 'product_color', 'field' => 'slug', 'terms' => $color]]
            );

            $colorsArray = array_merge($colorsArray, get_posts($args));

        }

        foreach($materials as $material){
            if(!$material){
                continue;
            }
            $args = array(
                'posts_per_page'   => -1,
                'offset'           => 0,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'product',
                'post_status'      => 'publish',
                'tax_query' => [['taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $material]]
            );

            $materialsArray = array_merge($materialsArray, get_posts($args));

        }
    }

    function isSamePosts($v1, $v2)
    {
        if($v1->ID == $v2->ID){
            return 0;
        }

        return ($v1->ID > $v2->ID)?1:-1;
    }

    if($postsArray && $hasColor && $hasMaterial){
        $intersections = array_uintersect($postsArray, $colorsArray, $materialsArray, 'isSamePosts');
    } elseif($postsArray && !$hasColor && !$hasMaterial){
        $intersections = $postsArray;
    } elseif($postsArray && !$hasColor && $hasMaterial) {
        $intersections = array_uintersect($postsArray, $materialsArray, 'isSamePosts');
    } elseif($postsArray && $hasColor && !$hasMaterial) {
        $intersections = array_uintersect($postsArray, $colorsArray, 'isSamePosts');
    } elseif(!$postsArray && $hasColor && $hasMaterial) {
        $intersections = array_uintersect($colorsArray, $materialsArray, 'isSamePosts');
    } elseif($hasColor && !$hasMaterial){
        $intersections = $colorsArray;
    } elseif(!$hasColor && $hasMaterial){
        $intersections = $materialsArray;
    } else {
        $intersections = [];
    }

    $postsArray = $intersections;
}


//var_dump($intersections);
?>
<?php if($postsArray){ ?>
    <?php foreach($postsArray as $post) {
        $tagModels = wp_get_post_tags($post->ID);
        $tags = [];
        foreach($tagModels as $tagModel){ $tags[] = $tagModel->name; }?>
        <?php if(!in_array('individual product', $tags)){ ?>
        <article id="post-<?php the_ID($post); ?>" <?php post_class('', $post); ?> data-color="<?=$productColor[0]->slug?>" data-cat="<?=$productCat[0]->slug?>">

            <!-- post thumbnail -->
            <?php if ( has_post_thumbnail($post)) : // Check if thumbnail exists ?>
                <a href="<?php the_permalink($post); ?>" title="<?php get_the_title($post); ?>">
                    <?php echo get_the_post_thumbnail($post, array(120,120)); // Declare pixel size you need inside the array ?>
                </a>
            <?php endif; ?>
            <!-- /post thumbnail -->

            <!-- post title -->
            <h2>
                <a href="<?php the_permalink($post); ?>" title="<?php echo get_the_title($post); ?>"><?php echo get_the_title($post); ?></a>
            </h2>
            <!-- /post title -->

            <!-- post details -->
            <!-- /post details -->

            <?php //html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

            <?php //edit_post_link(); ?>

        </article>
        <?php } ?>
    <?php } ?>
<!-- /article -->

<?php } else { ?>

<!-- article -->
	<article>
		<h2 class="results-h2"><?php _e( 'No Results. Please Try Again.', 'html5blank' ); ?></h2>
	</article>
<!-- /article -->

<?php } ?>


